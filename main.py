import os

import cv2

import configparser

import anomaly_detector as ad
import blob_detector as bd

config = configparser.ConfigParser()
config.read('./config/app.cfg')
function_aae = 'ANOMALY_IMAGE_DETECTOR'
function_blob = 'BLOB_DETECTOR'

aid = ad.AnomalyImageDetector(config, function_aae)


aid.aae_model_load()
aid.feature_map_load()

input_dir =  config[function_aae]['input_dir']

for file in os.listdir(input_dir): 
	try: 
		print("Image: (%s) is being processed"%file)
		#load image form input path 
		img = cv2.imread(input_dir + file,cv2.IMREAD_GRAYSCALE)
		#perform AAE anomaly prediction and combine result with Blob-Detector
		aid.prediction(img, file)
		print("Processing: Done")
	except Exception as e:
			print(e)
	except:
		raise

try: 
	aid.blob_prediction.result_to_csv()
	aid.result_to_csv()
except Exception as e:
		for err in e:
			print("Processing: Failed",err)
		pass
except:
	raise