import sys
import os
import glob
import csv

import numpy as np

import scipy as sc
from scipy.signal import argrelmax
from scipy.spatial import distance

import cv2
from matplotlib import pyplot as plt

from keras.models import load_model

#Import Blob-Detector prediction module
import blob_detector as bd


class AnomalyImageDetector(object):
    def __init__(self, config, function):
        self.function = function
        '''
        Insert necessary paramateres
        Configuration file [app.cfg]
        '''
        self.model_path = config[function]['model_path']
        self.feature_map_path = config[function]['feature_map_path']

        self.image_size = np.int(config[function]['image_size']) #fixed parameter
        self.preproc_x_size = np.int(config[function]['x_size']) #x_size
        self.preproc_y_size = np.int(config[function]['y_size']) #y_size
        self.preproc_filter_size = np.int(config[function]['filter_size']) #r
        self.preproc_gradation = np.int(config[function]['gradation']) # n
        self.preproc_threshold = np.float32(config[function]['threshold']) #thr
        self.aae_boundry = np.float32(config[function]['boundry'])

        self.result_report_path = config[function]['result_report_path']
        self.error_log_path = config[function]['error_log_path']
        
        self.__init_parameter_check('__init__')
        self.blob_prediction = bd.BlobDetector(config, "BLOB_DETECTOR")

        #empty frame for result appending
        self.results = []
    
    def __init_parameter_check(self, sub_function):
        '''
        Critical file existance checkeck. 
        Prediction Model, Feature Map are critical for prediction process.
        Missing any of them will result stopping process and displaying error message
        '''
        #empty frame for error messages appending
        error_list = []

        if not(os.path.exists(self.model_path)):
            error_type = "FileNotFoundError"
            error_msg = "Missing Prediction Model File"
            error_list.append([self.function,
                                sub_function,
                                error_type,
                                error_msg])
        if not(os.path.exists(self.feature_map_path)):
            error_type = "FileNotFoundError"
            error_msg = "Missing Feature Map File"
            error_list.append([self.function,
                                sub_function,
                                error_type,
                                error_msg])
        if len(error_list) > 0:
            self.__error_log_to_csv(error_list)
            raise Exception(error_list)
        pass
    '''
    Loading Prediction Model and Feature Map 
    '''
    def aae_model_load(self):
        self.model = load_model(self.model_path)
        pass
    
    def feature_map_load(self):
        self.feature_map = np.load(self.feature_map_path)
        self.feature_map_cov = np.cov(self.feature_map.T)
        pass

    def prediction(self, img, image_id):
        '''
        Anomaly prediction based on Adversarial Auto Encoder method
        Input: Processed image , Blob-Detector result 
        Output: Prediction result [result]
        This module is combined with result from BlobDetector class
        Prediction description:
             [1]: normal image , [0]: abnormal image
        '''
        sub_function = sys._getframe().f_code.co_name
        # ---------------------------------
        # error check
        # ---------------------------------
        self.__prediction_error_check(sub_function, image_id, img)

        # ---------------------------------
        # anomaly detection
        # ---------------------------------
        # usage of preprocessed image
        '''
        Note: This version does not contain preprocessing. Already preprocessed image are used for this demonstration.
        '''
        img = self.__preprocessing(img)
        
        #trigger Blob-Detector prediction
        blob_val = self.blob_prediction.prediction(img, image_id)

        img = cv2.resize (img, (self.image_size , self.image_size))
        pred = self.model.predict(np.array(img,dtype='float32').reshape(1,
                                                                        self.image_size,
                                                                        self.image_size,
                                                                        1)/255)[0,:]
        #calculate distance based mahalanobis metric from feature map and input image features
        dist = [distance.cdist(pred.reshape(1,
                                            pred.shape[0]), 
                                            self.feature_map, 
                                            metric='mahalanobis', 
                                            VI=np.linalg.pinv(self.feature_map_cov))[0]]
        #indirect sorting of distance arguments, return array of indices in same shape, sort in order
        dist_result = np.array([distance[np.argsort(distance)[10]] for distance in dist],dtype="float32")
        #calculation of distance result in relation to assigned boundry
        aae_result = 1 if dist_result <= self.aae_boundry else 0
        # final prediction assesment 
        result = 1 if (aae_result == 1) and (blob_val == 0) else 0
        self.results.append([image_id, str(result)])
        pass
	
    def __prediction_error_check(self, sub_function, image_id, img):
        '''
        Perform error check for input image format
        '''
        error_list = []
        # exclude incorrect file naming
        if not(type(image_id) == np.str):
            error_type = "TypeError"
            error_msg = "Input data type is wrong(parameter:image_id)"
            error_list.append([self.function,
                               sub_function,
                               error_type,
                               error_msg])
        #input value check
        if not(type(img)==np.ndarray):
            error_type = "TypeError"
            error_msg = "Input image has wrong values(image_id:{0})".format(image_id)
            error_list.append([self.function,
                               sub_function,
                               error_type,
                               error_msg])
        else:
        #input shape check
            if len(img.shape) != 2:
                error_type = "InputDataShapeError"
                error_msg = "Input data shape must be (a,b)(image_id:{0})".format(image_id)
                error_list.append([self.function,
                                    sub_function,
                                    error_type,
                                    error_msg])
            #input file format
            if not(img.dtype == np.uint8):
                error_type = "TypeError"
                error_msg = "Input image is not uint8 type(image_id:{0})".format(image_id)
                error_list.append([self.function,
                                    sub_function,
                                    error_type,
                                    error_msg])
        #confirm if there is any error on list
        if len(error_list) > 0:
            self.__error_log_to_csv(error_list)
            raise Exception(error_list)
        pass
    
    def __preprocessing(self, img):
        '''
		Convex Hull method used for extracting only area of interest.
        Image processing: Extraction hand image without background and arm bone
        Input: Original image
        Output: Extracted image of hand
        '''
        img = ImgFunction().gray_scaled(img.astype(np.uint8))
		
        #img = masking(img,256,256,8)
        return img
   
    def __error_log_to_csv(self, error_list):
        '''
        Input: (If any) error messages 
        Ouput: Save prediction report in designed path
        '''
        error_log_file = self.error_log_path
        write_row_flag = os.path.exists(error_log_file)
        with open(error_log_file, "a+", encoding="utf_8_sig") as file:
            writer = csv.writer(file)
            if not(write_row_flag):
                writer.writerow(["function", "sub_function", "error_type", "error"])
            for e in range(len(error_list)):
                writer.writerow(error_list[e])
        pass

    def result_to_csv(self):
        '''
        Input: result [image Id , precition]
        Ouput: Save prediction report in designed path
        '''
        result_csv_path = self.result_report_path
        write_row_flag = os.path.exists(result_csv_path)
        with open(result_csv_path, "a+", encoding="utf_8_sig") as file:
            writer = csv.writer(file)
            if not(write_row_flag):
                writer.writerow(["image_id","prediction"])
            for rec in self.results:
                writer.writerow(rec)
        pass
