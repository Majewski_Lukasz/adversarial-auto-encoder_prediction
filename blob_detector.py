import sys
import os
import csv

import numpy as np
from math import sqrt

import cv2
from skimage.morphology import dilation
from skimage.feature import blob_log


class BlobDetector(object):
    def __init__(self, config, function):
        self.function = function
        # preprocessing parameters
        self.filter_size = np.int(config[function]['blob_filter_size'])
        self.image_size = np.int(config[function]['blob_img_size'])
        # binalization parameters
        self.frame_horizontal_top = np.int(config[function]['frame_horizontal_top'])
        self.frame_horizontal_bottom = np.int(config[function]['frame_horizontal_bottom'])
        self.frame_vertical_top = np.int(config[function]['frame_vertical_top'])
        self.frame_vertical_bottom = np.int(config[function]['frame_vertical_bottom'])
        self.binarization_threshold = np.int(config[function]['binarization_threshold'])
        self.binarization_threshold_f = np.int(config[function]['binarization_threshold_f'])
        self.min_sigma = np.float(config[function]['blob_min_sigma'])
        self.min_sigma_f = np.float(config[function]['blob_min_sigma_f'])
        self.max_sigma = np.float(config[function]['blob_max_sigma'])
        self.max_sigma_f = np.float(config[function]['blob_max_sigma_f'])
        self.num_sigma = np.float(config[function]['blob_num_sigma'])
        self.num_sigma_f = np.float(config[function]['blob_num_sigma_f'])
        self.blob_threshold = np.float(config[function]['blob_threshold'])
        self.blob_threshold_f = np.float(config[function]['blob_threshold_f'])
        self.b_error_log_path = config[function]['b_error_log_path']
        self.b_result_report_path = config[function]['b_result_report_path']
        self.results = []

    def prediction(self, img, image_id):
        sub_function = sys._getframe().f_code.co_name
        '''
        Abnormal object detection based on Blob-Detector method
        Input: Preocessed image 
        Output: Count of abnormal objects [blob_val]
        blob_val  number different than 0 indicates existance of abnormality
        '''
        # ----------------------------------
        # parameter check
        #-----------------------------------
        self.__prediction_error_check(sub_function, img, image_id)
        # ----------------------------------
        # start prediction process
        # ----------------------------------
        # preprocess
        img = cv2.resize(img, (self.image_size,self.image_size))
        img = cv2.medianBlur(img,self.filter_size)
		
		#Cut frame form image for detailed inspection
        frame = img[self.frame_vertical_top:self.frame_vertical_bottom,
                    self.frame_horizontal_top:self.frame_horizontal_bottom]
        '''
        Note:
        In order to increase detection accuracy, 
        process is performed twice with adjusted parameters.
        '''
        #general detection of anomaly objects on whole input image
        numrows =  self.__blob_detector(img,
                                        dilation_flag=True,
                                        binalization_threshold=self.binarization_threshold,
                                        min_sigma=self.min_sigma,
                                        max_sigma=self.max_sigma,
                                        num_sigma=self.num_sigma,
                                        blob_threshold=self.blob_threshold)
        #more detailed detection performed on centered frame cut from input image
        numrows_f =  self.__blob_detector(frame,
                                          dilation_flag=False,
                                          binalization_threshold=self.binarization_threshold_f,
                                          min_sigma=self.min_sigma_f,
                                          max_sigma=self.max_sigma_f,
                                          num_sigma=self.num_sigma_f,
                                          blob_threshold=self.blob_threshold_f)		
        #sum up anomalies form whole image and frame
        blob_val = numrows + numrows_f
        self.results.append([image_id, str(blob_val)])
        return blob_val
    
    def __prediction_error_check(self, sub_function, img, image_id):
        error_list = []
        # exclude incorrect file naming
        if not(type(image_id) == np.str):
            error_type = "TypeError"
            error_msg = "Input data type is wrong(parameter:image_id)"
            error_list.append([self.function,
                               sub_function,
                               error_type,
                               error_msg])
        #input value check
        if not(type(img)==np.ndarray):
            error_type = "TypeError"
            error_msg = "Input image has wrong values(image_id:{0})".format(image_id)
            error_list.append([self.function,
                               sub_function,
                               error_type,
                               error_msg])
        else:
        #input shape check
            if len(img.shape) != 2:
                error_type = "InputDataShapeError"
                error_msg = "Input data shape must be (a,b)(image_id:{0})".format(image_id)
                error_list.append([self.function,
                               sub_function,
                               error_type,
                               error_msg])
            #input file format
            if not(img.dtype == np.uint8):
                #input image is not unit8 type file
                error_type = "TypeError"
                error_msg = "Input image is not uint8 type(image_id:{0})".format(image_id)
                error_list.append([self.function,
                               sub_function,
                               error_type,
                               error_msg])
        #confirm if there is any error on list
        if len(error_list) > 0:
            self.__error_log_to_csv(error_list)
            raise Exception(error_list)
        pass

    def __blob_detector(self, img, dilation_flag, binalization_threshold, min_sigma, max_sigma, num_sigma, blob_threshold):
        _, im_t = cv2.threshold(img, binalization_threshold, 255, cv2.THRESH_TOZERO)
        if dilation_flag:
           im_t = dilation(im_t,selem=None)
		#count abnormal objects from whole image
        blobs_log = blob_log(im_t,
                             min_sigma=min_sigma,
                             max_sigma=max_sigma,
                             num_sigma=num_sigma,
                             threshold=blob_threshold)
        blobs_log[:,2] = blobs_log[:,2] * sqrt(2)
        numrows = len(blobs_log)
        return numrows
    
    def __error_log_to_csv(self, error_list):
        error_log_file = self.b_error_log_path
        write_row_flag = os.path.exists(error_log_file)
        if not os.path.exists(error_log_file):
            with open(error_log_file, "a+", encoding="utf_8_sig") as file:
                writer = csv.writer(file)
                if not(write_row_flag):
                    writer.writerow(["function", "sub_function", "error_type", "error"])
                for e in range(len(error_list)):
                    writer.writerow(error_list[e])
            pass
    def result_to_csv(self):
        '''
        Input: result [image Id , blob evaluation]
        Ouput: Save prediction report in designed path
        '''
        result_csv_path = self.b_result_report_path
        write_row_flag = os.path.exists(result_csv_path)
        with open(result_csv_path, "a+", encoding="utf_8_sig") as file:
            writer = csv.writer(file)
            if not(write_row_flag):
                writer.writerow(["image_id","blob_evaluation"])
            for rec in self.results:
                writer.writerow(rec)
        pass